# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lars/RPiSystemInfo/src/lcdcontrol/lcdcontrol.cpp" "/home/lars/RPiSystemInfo/build/CMakeFiles/RPiSystemInfo.dir/src/lcdcontrol/lcdcontrol.cpp.o"
  "/home/lars/RPiSystemInfo/src/main.cpp" "/home/lars/RPiSystemInfo/build/CMakeFiles/RPiSystemInfo.dir/src/main.cpp.o"
  "/home/lars/RPiSystemInfo/src/processinfo/processinfo.cpp" "/home/lars/RPiSystemInfo/build/CMakeFiles/RPiSystemInfo.dir/src/processinfo/processinfo.cpp.o"
  "/home/lars/RPiSystemInfo/src/rpisysteminfo/rpisysteminfo.cpp" "/home/lars/RPiSystemInfo/build/CMakeFiles/RPiSystemInfo.dir/src/rpisysteminfo/rpisysteminfo.cpp.o"
  "/home/lars/RPiSystemInfo/src/weatherinfo/weatherinfo.cpp" "/home/lars/RPiSystemInfo/build/CMakeFiles/RPiSystemInfo.dir/src/weatherinfo/weatherinfo.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../include/nlohmann"
  "../src/lcdcontrol"
  "../src/processinfo"
  "../src/weatherinfo"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

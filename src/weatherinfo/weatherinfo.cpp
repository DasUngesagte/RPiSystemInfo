#include <iostream>
#include <curl/curl.h>
#include "json.hpp"  // Everything JSON-related
#include "weatherinfo.hpp"


const char *weatherinfo::Weather::URL =
        "https://maps.dwd.de/geoserver/dwd/ows"
        "?service=WFS&version=1.0.0&request=GetFeature&typeName="
        "dwd:Warnungen_Gemeinden&outputFormat=text%2Fjavascript";


// Get new _warnings
void weatherinfo::Weather::update(const std::vector<std::string> &locations) {

    // Init CURL:
    CURL *curl;
    CURLcode res;
    std::string read_buffer;
    curl = curl_easy_init();

    if (curl) {

        // Set it up and try to download
        curl_easy_setopt(curl, CURLOPT_URL, URL);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &read_buffer);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        if (res == CURLE_OK) {

            // Remove the "parseResponse(...)" - part:
            size_t cut_start = sizeof("parseResponse(") - 1;
            size_t cut_length = read_buffer.size() - cut_start - 1;
            read_buffer = read_buffer.substr(cut_start, cut_length);

            try {
                this->data = nlohmann::json::parse(read_buffer);
            } catch (nlohmann::json::parse_error &err) {
                // data stays null if exception occurs on first run
                // data keeps old information if exception occurs after
                // a prior successful run of update()
                std::cerr << err.what() << std::endl;
            }
        } else {
            std::cerr << "curl failed" << std::endl;
        }
    }

    // For a successful download:
    if (!data.is_null()) {
        search_warnings(locations);
    }
}


// Searches the json for weather warning for the given
// location
void weatherinfo::Weather::search_warnings(
        const std::vector<std::string> &locations) {

    // Clear all old _warnings:
    _warnings.clear();

    bool found = false;

    // DWD-json: features -> properties -> event/name/expires/description
    for (const nlohmann::json &feat : (this->data)["features"]) {

        if (feat["properties"].find("NAME") != feat["properties"].end()) {


            for (const auto &location : locations) {

                if (feat["properties"]["AREADESC"] == location ||
                        feat["properties"]["NAME"] == location) {
                    // AREADESC doesn't contain "Stadt" or "Gemeinde",
                    // so it's a bit easier to use

                    weatherinfo::Weather_Event event = {
                            feat["properties"]["EVENT"],
                            feat["properties"]["EXPIRES"],
                            feat["properties"]["DESCRIPTION"]};

                    for (auto &existing: _warnings) {

                        // Check for existing warnings:
                        if (location == existing.name) {
                            // Append Event:
                            existing.events.push_back(event);
                            found = true;
                            break;
                        }
                    }

                    if (!found) {

                        // Create new warning:
                        std::vector<Weather_Event> events(0);
                        events.push_back(event);
                        weatherinfo::Weather_Warning warning = {
                                feat["properties"]["AREADESC"], events};

                        // Append:
                        _warnings.push_back(warning);
                    }

                }

                found = false;
            }
        }
    }
}


// Needed for curl-callback
size_t weatherinfo::Weather::write_callback(void *contents, size_t size,
                                            size_t nmemb, void *userp) {
    ((std::string *) userp)->append((char *) contents, size * nmemb);
    return size * nmemb;
}

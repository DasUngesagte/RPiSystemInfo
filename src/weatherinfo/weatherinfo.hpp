#ifndef RPISYSTEMINFO_SRC_WEATHERINFO_WEATHERINFO_HPP_
#define RPISYSTEMINFO_SRC_WEATHERINFO_WEATHERINFO_HPP_

#include <string>  // string, surprisingly
#include <vector>  // vector, surpisingly as well
#include "json.hpp"  // all JSON-functionality


namespace weatherinfo {

    struct Weather_Event {
        std::string event;
        std::string expires;
        std::string description;
    };

    struct Weather_Warning {
        std::string name;  // Location
        std::vector<Weather_Event> events;
    };

    class Weather {

    public:
        Weather() : _warnings(0) {};

        void update(const std::vector<std::string> &locations);

        std::vector<Weather_Warning> get_warnings() const {
            return _warnings;
        };

    private:
        void search_warnings(const std::vector<std::string> &locations);

        static size_t write_callback(void *contents, size_t size,
                                     size_t nmemb, void *userp);

        nlohmann::json data;
        std::vector<Weather_Warning> _warnings;

        static const char *URL;
    };

}

#endif  // RPISYSTEMINFO_SRC_WEATHERINFO_WEATHERINFO_HPP_

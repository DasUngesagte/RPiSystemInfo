#include "rpisysteminfo/rpisysteminfo.hpp"

int main(int argc, char *argv[]) {

    std::vector<std::string> locations(0);

    for (size_t i = 1; i < argc; ++i) {
        locations.push_back(argv[i]);
    }

    auto info = systeminfo::RPiSystemInfo(locations, 15);

    info.mainloop();

    return 0;
}

#ifndef RPISYSTEMINFO_RPISYSTEMINFO_HPP
#define RPISYSTEMINFO_RPISYSTEMINFO_HPP

#include <chrono>
#include <csignal>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "../weatherinfo/weatherinfo.hpp"
#include "../lcdcontrol/lcdcontrol.hpp"

namespace systeminfo {

    class RPiSystemInfo {
    public:
        RPiSystemInfo(std::vector<std::string> locations,
                      short interval);

        void mainloop();

    private:
        lcd::LCD _lcd;

        std::vector<std::string> _locations;
        weatherinfo::Weather _weather;
        std::vector<weatherinfo::Weather_Warning> _warnings;
        std::chrono::steady_clock::time_point _lastupdate;
        volatile bool _isDone = false;
        static volatile bool EXITGRACEFULLY;
        std::chrono::minutes _interval;

        RPiSystemInfo() = delete;

        static void terminate(int signum);

        void page_weather();

        void update_weather();

        std::string replace_all(std::string str, const std::string &from,
                                const std::string &to);

        void page_proc();

        void page_resources();

        void page_cpuinfo();

        bool check_time();
    };
}

#endif //RPISYSTEMINFO_RPISYSTEMINFO_HPP

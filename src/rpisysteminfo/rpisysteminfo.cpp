#include "rpisysteminfo.hpp"

volatile bool systeminfo::RPiSystemInfo::EXITGRACEFULLY = false;

void systeminfo::RPiSystemInfo::terminate(int signum) {
    EXITGRACEFULLY = true;
}


void systeminfo::RPiSystemInfo::page_weather() {

    if (!_warnings.empty()) {
        for (const auto &warning_struct: _warnings) {

            // Don't go through every location when SIGTERM is called:
            if (EXITGRACEFULLY) break;

            // Build a string with all the current events:
            std::string event_texts = "***";
            std::string event_location = warning_struct.name;

            for (const auto &event: warning_struct.events) {
                event_texts += " " + event.event + ",";
            }

            // Get rid of the last comma:
            event_texts = event_texts.substr(0, event_texts.size() - 1);
            event_texts += (" ***");

            // Replace Umlauts:
            event_texts = replace_all(event_texts, "Ä", "Ae");
            event_texts = replace_all(event_texts, "ä", "ae");
            event_texts = replace_all(event_texts, "Ü", "Ue");
            event_texts = replace_all(event_texts, "ü", "ue");
            event_texts = replace_all(event_texts, "Ö", "Oe");
            event_texts = replace_all(event_texts, "ö", "oe");
            event_texts = replace_all(event_texts, "ß", "ss");
            event_location = replace_all(event_location, "Ä", "Ae");
            event_location = replace_all(event_location, "ä", "ae");
            event_location = replace_all(event_location, "Ü", "Ue");
            event_location = replace_all(event_location, "ü", "ue");
            event_location = replace_all(event_location, "Ö", "Oe");
            event_location = replace_all(event_location, "ö", "oe");
            event_location = replace_all(event_location, "ß", "ss");

            // Show it:
            _lcd.write_simple_line(event_location, 0);
            _lcd.write_scrolling_line(event_texts, 1, 3, 8, 250);
            _lcd.clear();
        }
    } else {
        _lcd.write_scrolling_line("*** Keine Warnung ***", 1, 3, 8, 250);
        _lcd.clear();
    }
}


void systeminfo::RPiSystemInfo::update_weather() {
    _weather.update(_locations);
    _lastupdate = std::chrono::steady_clock::now();
    _isDone = true;
}


void systeminfo::RPiSystemInfo::page_proc() {}


void systeminfo::RPiSystemInfo::page_resources() {}


void systeminfo::RPiSystemInfo::page_cpuinfo() {}


void systeminfo::RPiSystemInfo::mainloop() {

    // Register signal handler for SIG***
    signal(SIGTERM, terminate);
    signal(SIGINT, terminate);

    // First update:
    std::thread thread_one = std::thread(
            &systeminfo::RPiSystemInfo::update_weather, this);

    _lcd.clear();

    while (!EXITGRACEFULLY) {

        if (check_time()) {

            auto now = std::chrono::steady_clock::now();
            auto difference =
                    std::chrono::duration_cast<std::chrono::minutes>(now -
                                                                  _lastupdate);

            // Update when more than interval minutes have gone by:
            if (difference > _interval) {
                std::cout << "Updating" << std::endl;
                thread_one = std::thread(
                        &systeminfo::RPiSystemInfo::update_weather, this);
            }

            // Check whether the thread finished:
            if (_isDone) {
                std::cout << "Update finished" << std::endl;
                thread_one.join();
                _isDone = false;
                _warnings = _weather.get_warnings();  // Just a copy for simple
                // thread safety
            }

            // Show screens on lcd:
            page_weather();
            page_proc();
            page_resources();
            page_cpuinfo();

        }
    }

    if (EXITGRACEFULLY) {
        _lcd.clear();
        _lcd.backlight(false);
    }
}


systeminfo::RPiSystemInfo::RPiSystemInfo(std::vector<std::string> locations,
                                         short interval) :
        _lcd(), _weather(), _interval(interval) {
    _locations = locations;
    _lastupdate = std::chrono::steady_clock::now();
    _lcd.backlight(true);
}


// Helper
std::string systeminfo::RPiSystemInfo::replace_all(std::string str, const
std::string &from, const std::string &to) {
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}


// Helper
bool systeminfo::RPiSystemInfo::check_time() {
    time_t currentTime;
    struct tm *localTime;
    time(&currentTime);
    localTime = localtime(&currentTime);
    int hour = localTime->tm_hour;

    return hour < 23 && hour > 6;
}
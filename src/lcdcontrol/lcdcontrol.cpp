#include <chrono>
#include <iostream>
#include <thread>
#include "lcdcontrol.hpp"


lcd::LCD::LCD(int pinbase, int i2cAddress,
              int rows, int columns,
              int rs, int rw, int e,
              int bl, int d0, int d1,
              int d2, int d3) {

    wiringPiSetupGpio();
    int fd = pcf8574Setup(pinbase, i2cAddress);

    if (fd < 0) {
        std::cout << "I2C Setup failed" << std::endl;
        return;
    }

    _lcd_handle = lcdInit(rows, columns, 4, pinbase + rs, pinbase + e,
                          pinbase + d0, pinbase + d1, pinbase + d2,
                          pinbase + d3, 0, 0, 0, 0);
    _bl = pinbase + bl;
    _columns = columns;
    _rows = rows;

    // Turn the Display on:
    lcdDisplay(_lcd_handle, true);
};


void lcd::LCD::write_simple_line(std::string text, short row) {

    if (row > _rows) throw std::invalid_argument("Non-existing row!");

    lcdPosition(_lcd_handle, 0, row);
    lcdPuts(_lcd_handle, text.c_str());
};


void lcd::LCD::write_scrolling_line(std::string text,
                                    unsigned short row,
                                    unsigned short repeats,
                                    unsigned short padding_size,
                                    unsigned short delay) {

    if (row > _rows) throw std::invalid_argument("Non-existing row!");
    if (repeats < 1) throw std::invalid_argument("Only repeats > 0!");

    // Start with empty spaces:
    std::string complete_string(_columns, ' ');

    // Add text and padding:
    for(size_t i = 0; i < repeats - 1; ++i){
        complete_string.append(text + std::string(padding_size, ' '));
    }

    // Add the last text + fill with empty spaces for complete fade-out
    complete_string.append(text + std::string(_columns, ' '));

    for(size_t i = 0; i < complete_string.size() - _columns; ++i){
        std::string buffer_string = complete_string.substr(i, _columns);
        write_simple_line(buffer_string, row);
        std::this_thread::sleep_for(std::chrono::milliseconds(delay));
    }

    // Clear just the used line:
    std::string clear_string(_columns, ' ');
    lcdPuts(_lcd_handle, clear_string.c_str());

};


bool lcd::LCD::backlight(bool on) {

    // Dont send commands to fast
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    pinMode(_bl, OUTPUT);

    if (on) {
        digitalWrite(_bl, HIGH);
    } else {
        digitalWrite(_bl, LOW);
    }


};


void lcd::LCD::clear() {
    lcdClear(_lcd_handle);
}

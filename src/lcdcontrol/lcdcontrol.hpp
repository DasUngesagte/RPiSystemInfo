#ifndef RPISYSTEMINFO_SRC_LCDCONTROL_LCDCONTROL_HPP_
#define RPISYSTEMINFO_SRC_LCDCONTROL_LCDCONTROL_HPP_


#include <string>
#include <wiringPi.h>
#include <pcf8574.h>
#include <lcd.h>

namespace lcd {

    class LCD {

    private:
        int _lcd_handle;
        unsigned int _columns;
        unsigned int _rows;
        unsigned int _bl;

    public:
        LCD(int pinbase = 64, int i2cAddress = 0x3f,
            int rows = 2, int columns = 16,
            int rs = 0, int rw = 1, int e = 2,
            int bl = 3, int d0 = 4, int d1 = 5,
            int d2 = 6, int d3 = 7);

        void write_simple_line(std::string text, short line);

        void write_scrolling_line(std::string text,
                                  unsigned short row,
                                  unsigned short repeats,
                                  unsigned short padding_size,
                                  unsigned short delay);

        bool backlight(bool on);

        void clear();

    };
}

#endif  // RPISYSTEMINFO_SRC_LCDCONTROL_LCDCONTROL_HPP_

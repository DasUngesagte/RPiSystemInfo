# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/RPiSystemInfo/src/lcdcontrol/lcdcontrol.cpp" "/home/pi/RPiSystemInfo/buildpi/CMakeFiles/RPiSystemInfo.dir/src/lcdcontrol/lcdcontrol.cpp.o"
  "/home/pi/RPiSystemInfo/src/main.cpp" "/home/pi/RPiSystemInfo/buildpi/CMakeFiles/RPiSystemInfo.dir/src/main.cpp.o"
  "/home/pi/RPiSystemInfo/src/processinfo/processinfo.cpp" "/home/pi/RPiSystemInfo/buildpi/CMakeFiles/RPiSystemInfo.dir/src/processinfo/processinfo.cpp.o"
  "/home/pi/RPiSystemInfo/src/weatherinfo/weatherinfo.cpp" "/home/pi/RPiSystemInfo/buildpi/CMakeFiles/RPiSystemInfo.dir/src/weatherinfo/weatherinfo.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../include/nlohmann"
  "../src/lcdcontrol"
  "../src/processinfo"
  "../src/weatherinfo"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
